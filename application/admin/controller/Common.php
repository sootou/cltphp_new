<?php
namespace app\admin\controller;
use think\Db;
use think\Controller;
use think\facade\Request;
class Common extends Controller
{
    protected $mod,$role,$system,$nav,$menudata,$cache_model,$categorys,$module,$moduleid,$adminRules,$HrefId;
    public function initialize()
    {
        //判断管理员是否登录
        if (!session('aid')) {
            $this->redirect('admin/login/index');
        }
        define('MODULE_NAME',strtolower(request()->controller()));
        define('ACTION_NAME',strtolower(request()->action()));
        //权限管理
        //当前操作权限ID
        if(session('aid')!=1){
            $this->HrefId = db('auth_rule')->where('href',MODULE_NAME.'/'.ACTION_NAME)->value('id');
            //当前管理员权限
            $map['a.admin_id'] = session('aid');
            $rules=Db::table(config('database.prefix').'admin')->alias('a')
                ->join(config('database.prefix').'auth_group ag','a.group_id = ag.group_id','left')
                ->where($map)
                ->value('ag.rules');
            $this->adminRules = explode(',',$rules);
            if($this->HrefId){
                if(!in_array($this->HrefId,$this->adminRules)){
                    $this->error('您无此操作权限');
                }
            }
        }
        $this->cache_model=array('Module','AuthRule','Category','Posid','Field','System','cm');
        foreach($this->cache_model as $r){
            if(!cache($r)){
                savecache($r);
            }
        }
        $this->system = cache('System');
        $this->categorys = cache('Category');
        $this->module = cache('Module');
        $this->mod = cache('Mod');
        $this->rule = cache('AuthRule');
        $this->cm = cache('cm');
		
		//导航
		$module = Request::module();
		$controller = Request::controller();
		$action = Request::action(); 
        //声明数组
        $menus = array();
		//$authRule = cache('AuthRule');
		$authRule =  $authRule = db('auth_rule')->where('menustatus=1 AND status=1')->order('sort')->select();
		foreach ($authRule as $k=>$v){
		    if($v['status'] == 1){
			    $authRule[$k]['is_open'] = 0;//是否展开
				$authRule[$k]['is_light'] = 0;//是否高亮
				//判断是否展开和高亮
				$href1 = strtolower($v['href']);
				if(strpos($v['href'],'/')){
					$href1 = explode("/", $v['href']);
					$href1 = strtolower($href1['0']);
				}
				if(($href1 == strtolower($controller)) && ($v['pid'] == 0)){
					 $authRule[$k]['is_open'] = 1;
				}
				//echo strtolower($val['href'])."----";
				if(strtolower($v['href']) == strtolower($controller.'/'.$action)){
					 $authRule[$k]['is_light'] = 1;
				}
			}
		}
        foreach ($authRule as $key=>$val){
            $authRule[$key]['href'] = url($val['href']);
            if($val['pid']==0){
                if(session('aid')!=1){
                    if(in_array($val['id'],$this->adminRules)){
                        $menus[] = $val;
                    }
                }else{
                    $menus[] = $val;
                }
            }
        }
		
        foreach ($menus as $k=>$v){
            foreach ($authRule as $kk=>$vv){
                if($v['id']==$vv['pid']){
                    if(session('aid')!=1) {
                        if (in_array($vv['id'], $this->adminRules)) {
                            $menus[$k]['children'][] = $vv;
                        }
                    }else{
                        $menus[$k]['children'][] = $vv;
                    }
                }
            }
        }
		
		$this->assign('leftmenu',$menus);
    }
    //空操作
    public function _empty(){
        return $this->error('空操作，返回上次访问页面中...');
    }
}
