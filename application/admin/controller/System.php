<?php
namespace app\admin\controller;
use think\Db;
use clt\Menutree;
use clt\Leftnav;
use think\facade\Request;
class System extends Common
{   
    //protected $authRule;
    function initialize(){
        parent::initialize();
        $this->db = db('auth_rule');
        $this->authRule = db('auth_rule')->where('menustatus=1')->order('sort')->select();
        //声明数组
        $this->menus = array();
        foreach ($this->authRule as $key=>$val){
            $this->authRule[$key]['href'] = url($val['href']);
            $this->menus[] = $this->authRule[$key];
        }
    }
    /********************************站点管理*******************************/
    //站点设置
    public function system(){
        $id = input('param.id') ? input('param.id') : 1;
        $table = db('system');
        if(Request::post()) {
            $data = Request::except('file');
            print_r($data);
            if($table->where('id',1)->update($data)!==false) {
                savecache('System');
                $this->success('保存成功',url('system'));
            } else {
                $this->fail('保存失败',url('system'));
            }
        }else{
            $system = $table->find($id);
            $this->assign('system', $system);
            return $this->fetch();
        }
    }
    public function email(){
        if(Request::isAjax()) {
            $datas = input('post.');
            foreach ($datas as $k=>$v){
               Db::name('config')->where([['name','=',$k],['inc_type','=','smtp']])->update(['value'=>$v]);
            }
            return json(['code' => 1, 'msg' => '邮箱设置成功!', 'url' => url('system/email')]);
        }else{
            $smtp = Db::name('config')->where('inc_type','smtp')->select();
            $info = convert_arr_kv($smtp,'name','value');
            $this->assign('info', json_encode($info,true));
            return $this->fetch();
        }
    }
    public function trySend(){
        $sender = input('email');
        //检查是否邮箱格式
        if (!is_email($sender)) {
            return json(['code' => 0, 'msg' => '测试邮箱码格式有误']);
        }
        $arr = db('config')->where('inc_type','smtp')->select();
        $config = convert_arr_kv($arr,'name','value');
        $content = $config['test_eamil_info'];
        $send = send_email($sender, '测试邮件',$content);
        if ($send) {
            return json(['code' => 1, 'msg' => '邮件发送成功！']);
        } else {
            return json(['code' => 0, 'msg' => '邮件发送失败！']);
        }
    }

    public function menu(){
        $menus = $this->menus;
        $newarray = array();
        foreach($menus as $m => $r){
            $r['str_manage'] = '<a class="blue" title="添加子栏目" href="' . url('System/menuadd', array('pid' => $r['id'])) . '"> <i class="icon icon-plus"></i></a> | <a class="green" href="' . url('System/menuedit', array('id' => $r['id'])) . '" title="修改"><i class="icon icon-pencil2"></i></a> | <a class="red" href="javascript:del(\'' . $r['id'] . '\')" title="删除"><i class="icon icon-bin"></i></a> ';
            $r['dis'] = $r['status'] == 1 ? '<font color="green">显示</font>' : '<font color="red">不显示</font>';
            $newarray[$m] = $r;
        }
        
        $str = "<tr><td class='visible-lg visible-md'><input type='text' size='10' data-id='\$id' value='\$sort' class='layui-input list_order'></td>";
        $str .= "<td class='visible-lg visible-md'>\$id</td>";
        $str .= "<td class='text-left'>\$spacer<a href='\$href' class='green' title='查看内容'>\$title </a>&nbsp;</td>";
        $str .= "<td class='visible-lg visible-md'>\$href</td>";
        $str .= "<td class='visible-lg visible-md'>\$icon</td>";
        $str .= "<td class='visible-lg visible-md'>\$dis</td>";
        $str .= "<td>\$str_manage</td></tr>";
        $tree = new Menutree ($newarray);
        $tree->icon = array('&nbsp;&nbsp;&nbsp;│  ', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $categorys = $tree->get_menu(0, $str);
        $this->assign('menus',$newarray);
        $this->assign('categorys',$categorys);
        return $this->fetch();
    }
    /*
    ** 编辑栏目
    */
    public function menuedit(){
        $id = input('id');
        $menus = $this->menus;
        $menu = db('auth_rule')->where('id',$id)->find();//当前栏目
        $newarray = array();
        foreach($menus as $m => $r){
            if($menu['pid'] == 0){
                 $r['selected'] = $r['id'] == $menu['pid'] ? 'selected' : '';//当前栏目高亮
            }else{
                $r['selected'] = $r['id'] == $menu['pid'] ? 'selected' : '';//高亮父栏目
            }
            $newarray[$m] = $r;
        }
        
        $str  = "<option value='\$id' \$selected>\$spacer \$title</option>";
        $tree = new Menutree ($newarray);
        $tree->icon = array('&nbsp;&nbsp;&nbsp;│  ', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $categorys = $tree->get_menu(0, $str);
        if($menu['pid'] == 0){
            $str2  = "<option value='0'> 作为一级菜单</option>";
        }else{
            $str2  = "<option value='0' selected> 作为一级菜单</option>";
        }
        $categorys = $str2.$categorys;
        $this->assign('categorys',$categorys);
        $this->assign('menu', $menu);
        $this->assign('href', $href);
        $this->assign('menutitle',$menu['title'].'编辑');
        return $this->fetch();
    }

    /*
    ** 增加栏目
    */
    public function menuadd(){
        $pid = input('pid');
        $menus = $this->menus;
        $newarray = array();
        $str2 = '';
        foreach($menus as $m => $r){
            if(empty($pid) || $pid != 0){
                $r['selected'] = $r['id'] == $menu['pid'] ? 'selected' : '';//当前栏目高亮
            }
            $newarray[$m] = $r;
        }
        
        $str  = "<option value='\$id' \$selected>\$spacer \$title</option>";
        $tree = new Menutree ($newarray);
        $tree->icon = array('&nbsp;&nbsp;&nbsp;│  ', '&nbsp;&nbsp;&nbsp;├─ ', '&nbsp;&nbsp;&nbsp;└─ ');
        $tree->nbsp = '&nbsp;&nbsp;&nbsp;';
        $categorys = $tree->get_menu(0, $str);
        if($pid == 0 || empty($pid)){
            $str2  = "<option value='0' selected> 作为一级菜单</option>";
        }
        $categorys = $str2.$categorys;

        $this->assign('categorys', $categorys);
        $this->assign('now', time());
        $this->assign('menutitle','添加菜单');
        return $this->fetch();
    }
	/*
	** 栏目更新
	*/
	public function menuupdate(){
	    $data = Request::except('file');
	    $id = $data['id'];
	    $data['sort'] = $data['sort'] ? intval($data['sort']) : 0;
        $data['addtime'] = empty($data['addtime']) ? time() : strtotime($data['addtime']);
	    $authRule = db('auth_rule');
        if(empty($id) || $id == ""){
	        $res = $authRule->insert($data);
            if($res!== false){
                $this->success("添加成功",url('menu'));
            }else{
                $this->error("添加失败");  
            }
	    }else{
	        $res = $authRule->update($data);
            if($res!== false){
                $this->success("修改成功",url('menu'));
            }else{
                $this->error("修改失败");  
            }
	    }
	}
	
    /*
    ** 删除菜单
     */
    public function menudel(){
        $id = input('param.id');
        $menuinfo = $this->db->where('pid',$id)->select();
        $scount = count($menuinfo);
        if($scount){
            $result['info'] = '请先删除其子菜单!';
            $result['status'] = 0;
            return $result;
            die();
        }
        $res = $this->db->where('id',$id)->delete();
        if($res){
            $result['info'] = '栏目删除成功!';
            $result['url'] = url('menu');
            $result['status'] = 1;
        }else{
            $result['info'] = '栏目删除失败!';
            $result['url'] = url('menu');
            $result['status'] = 0;
        }
        return $result;
    }
    
	/*
	** 菜单排序
	*/
	public function modOrder(){
        $data = input('post.');
		$authRule = db('auth_rule');
        $res = $authRule->update($data);
		$flag = $res ? 1 : 0;
		$msg = $res ? '排序成功' : '排序失败';
        $result = ['msg' => $msg, 'code' => $flag,'url'=>url('menu')];
        //savecache('Category');
        //cache('cate', NULL);
        return $result;
    }
	

}
