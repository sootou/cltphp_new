<?php /*a:3:{s:62:"G:\phpstudy\mycltphp\application\admin\view\content/lists.html";i:1548386523;s:60:"G:\phpstudy\mycltphp\application\admin\view\common/head.html";i:1547648804;s:60:"G:\phpstudy\mycltphp\application\admin\view\common/foot.html";i:1547648788;}*/ ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/mycltphp/public/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/mycltphp/public/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/mycltphp/public/static/common/css/font.css" media="all">
    <script>var ROOT = "/mycltphp/public";</script>
    <script type="text/javascript" src="/mycltphp/public/static/plugins/layui/layui.js"></script>
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
    <div class="layui-form" style="bottom: 0; margin-left:0px;">
        <fieldset class="layui-elem-field layui-field-title">
            <legend><?php echo lang('list'); ?></legend>
        </fieldset>
        <div class="layui-form layui-border-box layui-table-view admin-main layui-anim" lay-filter="LAY-table-1" lay-id="content">
             <div class="layui-table-tool">
                <div class="layui-table-tool-temp"> <a href="/admin/article/add/catid/5.html" class="layui-btn layui-btn-sm">添加文章</a>
                  <button type="button" class="layui-btn layui-btn-danger layui-btn-sm" id="delAll">批量删除</button>
                  <a href="/admin/category/index.html" class="layui-btn layui-btn-normal layui-btn-sm">返回栏目</a> </div>
                <div class="layui-table-tool-self">
                  <div class="layui-inline" title="筛选列" lay-event="LAYTABLE_COLS"><i class="layui-icon layui-icon-cols"></i></div>
                  <div class="layui-inline" title="导出" lay-event="LAYTABLE_EXPORT"><i class="layui-icon layui-icon-export"></i></div>
                  <div class="layui-inline" title="打印" lay-event="LAYTABLE_PRINT"><i class="layui-icon layui-icon-print"></i></div>
                </div>
           </div>
          <div class="layui-table-box">
              <style>
                  .layui-table thead th { text-align:center;}
                  .layui-table-view .layui-table td,.layui-table-view .layui-table tr { padding:5px;}
				  .layui-form-checkbox[lay-skin=primary] { padding-left:5px;}
              </style>
              <table class="layui-table" style="width:100%;">
                <thead>
                  <tr>
                    <th style="width:40px;"><input type="checkbox" title="" id="did[]"  lay-skin="primary"  name="listid[]" value="<?php echo htmlentities($r['did']); ?>" /></th>
                    <th width="4%">编号</th>
                    <th width="7%">排序</th>
                    <th width="50%">标题</th>
                    <th width="7%">点击</th>
                    <th width="18%">添加时间</th>
                    <th style="width:130px;">操作</th>
                  </tr>
                </thead>
                <tbody>
                <?php if(is_array($list) || $list instanceof \think\Collection || $list instanceof \think\Paginator): $i = 0; $__LIST__ = $list;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$v): $mod = ($i % 2 );++$i;?>
                  <tr>
                    <td style="text-align:center;width:40px;"><input type="checkbox" title="" id="did[]"  lay-skin="primary"  name="listid[]" value="<?php echo htmlentities($v['id']); ?>" /></td>
                    <td style="text-align:center;"><?php echo htmlentities($v['id']); ?></td>
                    <td style="text-align:center;"><input name="50" data-id="50" class="list_order layui-input" value="<?php echo htmlentities($v['sort']); ?>" size="10"></td>
                    <td><?php echo htmlentities($v['title']); ?></td>
                    <td align="center"><?php echo htmlentities($v['hits']); ?></td>
                    <td><?php echo htmlentities(date('Y-m-d H:i:s',!is_numeric($v['updatetime'])? strtotime($v['updatetime']) : $v['updatetime'])); ?></td>
                    <td align="center"><div class="layui-btn-group" style="width:130px;">
                     <a href="https://show.cltphp.com/newsInfo-5-50.html" target="_blank" class="layui-btn layui-btn-xs layui-btn-normal">预览</a> 
                     <a href="/admin/article/edit.html?id=50&amp;catid=5" class="layui-btn layui-btn-xs">编辑</a> 
                     <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a> </div></td>
                  </tr>
                  <?php endforeach; endif; else: echo "" ;endif; ?>
                </tbody>
              </table>
          </div>
          <div class="c"></div>
          <div id="pages"><?php echo $list; ?></div>
        </div>
    </div><!--内框-->
<script>var ROOT = "/mycltphp/public";</script>


<script>
    layui.use('table', function() {
        var table = layui.table, $ = layui.jquery;
        
        //搜索
        $('#search').on('click', function () {
            var key = $('#key').val();
            if ($.trim(key) === '') {
                layer.msg('<?php echo lang("pleaseEnter"); ?>关键字！', {icon: 0});
                return;
            }
            tableIn.reload({ page: {page: 1}, where: {key: key,catid:'<?php echo input("catid"); ?>'} });
        });
        $('body').on('blur','.list_order',function() {
            var id = $(this).attr('data-id');
            var sort = $(this).val();
            var loading = layer.load(1, {shade: [0.1, '#fff']});
            $.post('<?php echo url("listorder"); ?>',{id:id,sort:sort,catid:'<?php echo input("catid"); ?>'},function(res){
                layer.close(loading);
                if(res.code === 1){
                    layer.msg(res.msg, {time: 1000, icon: 1}, function () {
                        location.href = res.url;
                    });
                }else{
                    layer.msg(res.msg,{time:1000,icon:2});
                }
            })
        });
        table.on('tool(list)', function(obj) {
            var data = obj.data;
            if(obj.event === 'del'){
                layer.confirm('您确定要删除该内容吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo url('listDel'); ?>",{id:data.id},function(res){
                        layer.close(loading);
                        if(res.code===1){
                            layer.msg(res.msg,{time:1000,icon:1});
                            tableIn.reload({where:{catid:'<?php echo input("catid"); ?>'}});
                        }else{
                            layer.msg('操作失败！',{time:1000,icon:2});
                        }
                    });
                    layer.close(index);
                });
            }
        });
        $('body').on('click','#delAll',function() {
            layer.confirm('确认要删除选中的内容吗？', {icon: 3}, function(index) {
                layer.close(index);
                var checkStatus = table.checkStatus('content'); //content即为参数id设定的值
                var ids = [];
                $(checkStatus.data).each(function (i, o) {
                    ids.push(o.id);
                });
                var loading = layer.load(1, {shade: [0.1, '#fff']});
                $.post("<?php echo url('delAll'); ?>", {ids: ids,catid:'<?php echo input("catid"); ?>'}, function (data) {
                    layer.close(loading);
                    if (data.code === 1) {
                        layer.msg(data.msg, {time: 1000, icon: 1});
                        tableIn.reload({where:{catid:'<?php echo input("catid"); ?>'}});
                    } else {
                        layer.msg(data.msg, {time: 1000, icon: 2});
                    }
                });
            });
        })
    });
</script>