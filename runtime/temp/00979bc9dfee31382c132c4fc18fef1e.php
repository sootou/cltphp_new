<?php /*a:4:{s:60:"G:\phpstudy\mycltphp\application\admin\view\index/index.html";i:1547649011;s:60:"G:\phpstudy\mycltphp\application\admin\view\common/head.html";i:1547648804;s:60:"G:\phpstudy\mycltphp\application\admin\view\common/left.html";i:1548148888;s:60:"G:\phpstudy\mycltphp\application\admin\view\common/foot.html";i:1547648788;}*/ ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/mycltphp/public/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/mycltphp/public/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/mycltphp/public/static/common/css/font.css" media="all">
    <script>var ROOT = "/mycltphp/public";</script>
    <script type="text/javascript" src="/mycltphp/public/static/plugins/layui/layui.js"></script>
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<script>
    var ADMIN = '/mycltphp/public/static/admin';
    //var navs = <?php echo $menus; ?>;
    var ROOT = '/mycltphp/public'
</script>
<div class="layui-layout layui-layout-admin">
    <div class="layui-header header">
        <div class="layui-main">
            <div class="admin-login-box">
                <a class="logo" style="left: 0;" href="<?php echo url('admin/index/index'); ?>">
                    <span style="font-size: 22px;"><?php echo config('sys_name'); ?></span>
                </a>
                <div class="admin-side-toggle fs1">
                    <span class="icon icon-menu"></span>
                </div>
                <div class="admin-side-full">
                    <span class="icon icon-enlarge"></span>
                </div>
            </div>

            <ul class="layui-nav admin-header-item" lay-filter="side-top-right">
                <li class="layui-nav-item" id="cache">
                    <a href="javascript:;"><?php echo lang('clearCache'); ?></a>
                </li>
                <li class="layui-nav-item">
                    <a href="<?php echo url(config('app.default_module').'/index/index'); ?>" target="_blank"><?php echo lang('home'); ?></a>
                </li>
                <li class="layui-nav-item">
                    <a class="name" href="javascript:;">主题</a>
                    <dl class="layui-nav-child">
                        <dd data-skin="0"><a href="javascript:;">默认</a></dd>
                        <dd data-skin="1"><a href="javascript:;">纯白</a></dd>
                        <dd data-skin="2"><a href="javascript:;">蓝白</a></dd>
                    </dl>
                </li>
                <li class="layui-nav-item">
                    <a href="javascript:;" class="admin-header-user">
                        <img src="<?php echo session('avatar'); ?>" class="layui-nav-img" />
                        <span><?php echo session('username'); ?></span>
                    </a>
                    <dl class="layui-nav-child">
                        <dd>
                            <a href="<?php echo url('index/logout'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i><?php echo lang('logout'); ?></a>
                        </dd>
                    </dl>
                </li>
            </ul>
            <ul class="layui-nav admin-header-item-mobile">
                <li class="layui-nav-item">
                    <a href="<?php echo url(config('app.default_module').'/index/index'); ?>" target="_blank"><?php echo lang('home'); ?></a>
                </li>
                <li class="layui-nav-item">
                    <a href="<?php echo url('index/logout'); ?>"><i class="fa fa-sign-out" aria-hidden="true"></i> <?php echo lang('logout'); ?></a>
                </li>
            </ul>
        </div>
    </div>
    <div class="layui-side layui-bg-black" id="admin-side"><!--id="admin-navbar-side"-->
        <div class="layui-side-scroll" lay-filter="side">
             <ul class="layui-nav layui-nav-tree beg-navbar">
  <?php if(is_array($leftmenu) || $leftmenu instanceof \think\Collection || $leftmenu instanceof \think\Paginator): $i = 0; $__LIST__ = $leftmenu;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav): $mod = ($i % 2 );++$i;?>
  <li class="layui-nav-item <?php if(($nav['is_open'] == 1)): ?>layui-nav-itemed<?php endif; ?>"><a href="javascript:;"><i class="icon <?php echo htmlentities($nav['icon']); ?>" aria-hidden="true" data-icon="icon-cogs"></i><cite><?php echo htmlentities($nav['title']); ?></cite><span class="layui-nav-more"></span></a>
    <dl class="layui-nav-child">
      <?php if(is_array($nav['children']) || $nav['children'] instanceof \think\Collection || $nav['children'] instanceof \think\Paginator): $i = 0; $__LIST__ = $nav['children'];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$nav2): $mod = ($i % 2 );++$i;?>
      <dd title="<?php echo htmlentities($nav2['title']); ?>" <?php if(($nav2['is_light'])): ?>class="layui-this"<?php endif; ?>><a href="<?php echo htmlentities($nav2['href']); ?>" data-url="<?php echo htmlentities($nav2['href']); ?>" target='main'><cite><?php echo htmlentities($nav2['title']); ?></cite></a></dd>
      <?php endforeach; endif; else: echo "" ;endif; ?>
    </dl>
  </li>
  <?php endforeach; endif; else: echo "" ;endif; ?>
  <span class="layui-nav-bar" style="top: 382.5px; height: 0px; opacity: 0;"></span>
</ul>

        </div>
    </div>
    <div class="layui-body" style="bottom: 0;border-left: solid 2px #1AA094;" id="admin-body">
        <div class="layui-tab admin-nav-card layui-tab-brief" lay-filter="admin-tab">
            <ul class="layui-tab-title">
                <li class="layui-this">
                    <i class="icon icon-earth" aria-hidden="true"></i>
                    <cite>控制面板</cite>
                </li>
            </ul>
            <div class="layui-tab-content" style="min-height: 150px; padding: 0;">
                <div class="layui-tab-item layui-show">
                    <iframe name='main' src="<?php echo url('main'); ?>"></iframe>
                </div>
            </div>
        </div>
    </div>
    <div class="layui-footer footer footer-demo" id="admin-footer">
        <div class="layui-main">
            <p>2017 &copy;
                <a href="http://www.cltphp.com/">www.cltphp.com</a> Apache Licence 2.0
            </p>
        </div>
    </div>
    <div class="site-tree-mobile layui-hide">
        <i class="layui-icon">&#xe602;</i>
    </div>
    <div class="site-mobile-shade"></div>
    <script>var ROOT = "/mycltphp/public";</script>


    <script src="/mycltphp/public/static/admin/js/index.js"></script>
    <script>
        layui.use('layer',function(){
            var $ = layui.jquery, layer = layui.layer;
            $('#cache').click(function () {
                document.cookie="skin=;expires="+new Date().toGMTString();
                layer.confirm('确认要清除缓存？', {icon: 3}, function () {
                    $.post('<?php echo url("clear"); ?>',function (data) {
                        layer.msg(data.info, {icon: 6}, function (index) {
                            layer.close(index);
                            window.location.href = data.url;
                        });
                    });
                });
            });
			$(".layui-nav-tree").find("li").find("dl").click(function(){
		       $(this).parent("li").addClass("layui-nav-itemed").siblings().removeClass("layui-nav-itemed");
		    });
        })
    </script>
</div>
</body>
</html>