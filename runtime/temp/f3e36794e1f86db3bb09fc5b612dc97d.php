<?php /*a:3:{s:61:"G:\phpstudy\mycltphp\application\admin\view\plugin/index.html";i:1545875458;s:60:"G:\phpstudy\mycltphp\application\admin\view\common/head.html";i:1547648804;s:60:"G:\phpstudy\mycltphp\application\admin\view\common/foot.html";i:1547648788;}*/ ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo config('sys_name'); ?>后台管理</title>
    <meta name="renderer" content="webkit">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <meta name="apple-mobile-web-app-status-bar-style" content="black">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="format-detection" content="telephone=no">
    <link rel="stylesheet" href="/mycltphp/public/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/mycltphp/public/static/admin/css/global.css" media="all">
    <link rel="stylesheet" href="/mycltphp/public/static/common/css/font.css" media="all">
    <script>var ROOT = "/mycltphp/public";</script>
    <script type="text/javascript" src="/mycltphp/public/static/plugins/layui/layui.js"></script>
</head>
<body class="skin-<?php if(!empty($_COOKIE['skin'])){echo $_COOKIE['skin'];}else{echo '0';setcookie('skin','0');}?>">
<div class="admin-main layui-anim layui-anim-upbit">
    <fieldset class="layui-elem-field layui-field-title">
        <legend>登录插件</legend>
    </fieldset>
    <table class="layui-table" id="list" lay-filter="list"></table>
</div>
<script>var ROOT = "/mycltphp/public";</script>


<script type="text/html" id="action">
    {{# if(d.status==0){ }}
    <a class="layui-btn layui-btn-xs" lay-event="install">一键安装</a>
    {{# }else{  }}
    <a class="layui-btn layui-btn-xs"  href="<?php echo url('set'); ?>?type={{d.type}}&code={{d.code}}" title="配置">配置</a>
    <a class="layui-btn layui-btn-xs layui-btn-danger" lay-event="install">卸载</a>
    {{# } }}
</script>
<script>
    layui.use('table', function() {
        var table = layui.table, $ = layui.jquery;
        var tableIn = table.render({
            id: 'login',
            elem: '#list',
            url: '<?php echo url("index"); ?>',
            method: 'post',
            cols: [[
                {field: 'name', title: '插件名称', width: 120,fixed: true},
                {field: 'type', align: 'center', title: '类别', width: 120},
                {field: 'code', align: 'center', title: 'CODE', width: 120},
                {field: 'desc', title: '插件描述', width:400},
                {width: 160, align: 'center', toolbar: '#action'}
            ]]
        });
        table.on('tool(list)', function(obj) {
            var data = obj.data;
            if (obj.event === 'install') {
                loading = layer.load(1, {shade: [0.1, '#fff']});
                var install = data.status===1?0:1;
                var type = data.type,code=data.code;
                $.post('<?php echo url("install"); ?>', {'type':type,code:code,install:install}, function (res) {
                    layer.close(loading);
                    if (res.code == 1) {
                        layer.msg(res.msg,{time:1000,icon:1});
                        tableIn.reload();
                    } else {
                        layer.msg(res.msg, {time: 1000, icon: 2});
                        return false;
                    }
                })
            }
        })
    });
</script>