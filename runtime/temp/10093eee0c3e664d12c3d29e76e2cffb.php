<?php /*a:1:{s:63:"G:\phpstudy\mycltphp\application\admin\view\content/search.html";i:1548231870;}*/ ?>
<div class="admin-main layui-anim layui-anim-upbit">
    <div class="layui-body" style="bottom: 0;border-left: solid 2px #1AA094;">
        <fieldset class="layui-elem-field layui-field-title">
            <legend><?php echo lang('list'); ?></legend>
        </fieldset>
        <div class="layui-form layui-border-box layui-table-view admin-main layui-anim" lay-filter="LAY-table-1" lay-id="content">
             <div class="layui-table-tool">
                <div class="layui-table-tool-temp"> <a href="/admin/article/add/catid/5.html" class="layui-btn layui-btn-sm">添加</a>
                  <button type="button" class="layui-btn layui-btn-danger layui-btn-sm" id="delAll">批量删除</button>
                  <a href="/admin/category/index.html" class="layui-btn layui-btn-normal layui-btn-sm">返回栏目</a> </div>
                <div class="layui-table-tool-self">
                  <div class="layui-inline" title="筛选列" lay-event="LAYTABLE_COLS"><i class="layui-icon layui-icon-cols"></i></div>
                  <div class="layui-inline" title="导出" lay-event="LAYTABLE_EXPORT"><i class="layui-icon layui-icon-export"></i></div>
                  <div class="layui-inline" title="打印" lay-event="LAYTABLE_PRINT"><i class="layui-icon layui-icon-print"></i></div>
                </div>
           </div>
          <div class="layui-table-box">
              <style>
                  .layui-table thead th { text-align:center;}
                  .layui-table-view .layui-table td,.layui-table-view .layui-table tr { padding:5px;}
              </style>
              <table class="layui-table" style="width:100%;">
                <thead>
                  <tr>
                    <th><input type="checkbox" title="" id="did[]"  lay-skin="primary"  name="listid[]" value="<?php echo htmlentities($r['did']); ?>" /></th>
                    <th>编号</th>
                    <th>排序</th>
                    <th>标题</th>
                    <th>点击</th>
                    <th>添加时间</th>
                    <th>操作</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td style="text-align:center;"><input type="checkbox" title="" id="did[]"  lay-skin="primary"  name="listid[]" value="<?php echo htmlentities($r['did']); ?>" /></td>
                    <td style="text-align:center;">50</td>
                    <td style="text-align:center;"><input name="50" data-id="50" class="list_order layui-input" value=" 0" size="10"></td>
                    <td>test1</td>
                    <td>33</td>
                    <td>2019-01-05 17:24:08</td>
                    <td><div class="layui-btn-group" style="width:135px;">
                     <a href="https://show.cltphp.com/newsInfo-5-50.html" target="_blank" class="layui-btn layui-btn-xs layui-btn-normal">预览</a> 
                     <a href="/admin/article/edit.html?id=50&amp;catid=5" class="layui-btn layui-btn-xs">编辑</a> 
                     <a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="del">删除</a> </div></td>
                  </tr>
                </tbody>
              </table>
          </div>
        </div>
    </div><!--内框-->
</div>
<script type="text/javascript" src="/mycltphp/public/static/common/js/jquery.2.1.1.min.js"></script>
<link rel="stylesheet" href="/mycltphp/public/static/plugins/treeview/css/jquery.treeview.css" type="text/css" />
<script type="text/javascript" src="/mycltphp/public/static/plugins/treeview/jquery.cookie.js"></script>
<script type="text/javascript" src="/mycltphp/public/static/plugins/treeview/jquery.treeview.js"></script>
<script type="text/javascript">
   $("#tree").treeview();
</script>
<script>
    layui.use('table', function() {
        var table = layui.table, $ = layui.jquery;
        
        //搜索
        $('#search').on('click', function () {
            var key = $('#key').val();
            if ($.trim(key) === '') {
                layer.msg('<?php echo lang("pleaseEnter"); ?>关键字！', {icon: 0});
                return;
            }
            tableIn.reload({ page: {page: 1}, where: {key: key,catid:'<?php echo input("catid"); ?>'} });
        });
        $('body').on('blur','.list_order',function() {
            var id = $(this).attr('data-id');
            var sort = $(this).val();
            var loading = layer.load(1, {shade: [0.1, '#fff']});
            $.post('<?php echo url("listorder"); ?>',{id:id,sort:sort,catid:'<?php echo input("catid"); ?>'},function(res){
                layer.close(loading);
                if(res.code === 1){
                    layer.msg(res.msg, {time: 1000, icon: 1}, function () {
                        location.href = res.url;
                    });
                }else{
                    layer.msg(res.msg,{time:1000,icon:2});
                }
            })
        });
        table.on('tool(list)', function(obj) {
            var data = obj.data;
            if(obj.event === 'del'){
                layer.confirm('您确定要删除该内容吗？', function(index){
                    var loading = layer.load(1, {shade: [0.1, '#fff']});
                    $.post("<?php echo url('listDel'); ?>",{id:data.id},function(res){
                        layer.close(loading);
                        if(res.code===1){
                            layer.msg(res.msg,{time:1000,icon:1});
                            tableIn.reload({where:{catid:'<?php echo input("catid"); ?>'}});
                        }else{
                            layer.msg('操作失败！',{time:1000,icon:2});
                        }
                    });
                    layer.close(index);
                });
            }
        });
        $('body').on('click','#delAll',function() {
            layer.confirm('确认要删除选中的内容吗？', {icon: 3}, function(index) {
                layer.close(index);
                var checkStatus = table.checkStatus('content'); //content即为参数id设定的值
                var ids = [];
                $(checkStatus.data).each(function (i, o) {
                    ids.push(o.id);
                });
                var loading = layer.load(1, {shade: [0.1, '#fff']});
                $.post("<?php echo url('delAll'); ?>", {ids: ids,catid:'<?php echo input("catid"); ?>'}, function (data) {
                    layer.close(loading);
                    if (data.code === 1) {
                        layer.msg(data.msg, {time: 1000, icon: 1});
                        tableIn.reload({where:{catid:'<?php echo input("catid"); ?>'}});
                    } else {
                        layer.msg(data.msg, {time: 1000, icon: 2});
                    }
                });
            });
        })
    });
</script>