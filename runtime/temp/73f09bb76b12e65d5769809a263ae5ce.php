<?php /*a:1:{s:60:"G:\phpstudy\mycltphp\application\admin\view\login/index.html";i:1547806114;}*/ ?>
<!DOCTYPE html>
<html lang="zh_cn">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
    <title>登录</title>
    <link rel="stylesheet" href="/mycltphp/public/static/plugins/layui/css/layui.css" media="all" />
    <link rel="stylesheet" href="/mycltphp/public/static/admin/css/login.css" />
    <link rel="stylesheet" href="/mycltphp/public/static/common/css/font.css" />
    <style>
        #chkcode{ position:absolute; top:10px; right:120px;}
    </style>
</head>
<body class="beg-login-bg">
<div class="container login">
    <div class="content">
        <div id="large-header" class="large-header">
            <canvas id="demo-canvas"></canvas>
            <div class="main-title">
                <div class="beg-login-box">
                    <header>
                        <h1><?php echo config('sys_name'); ?>后台登录</h1>
                    </header>
                    <div class="beg-login-main">
                        <form class="layui-form layui-form-pane" method="post">
                            <div class="layui-form-item">
                                <label class="beg-login-icon fs1">
                                    <span class="icon icon-user"></span>
                                </label>
                                <input type="text" name="username" lay-verify="required" placeholder="这里输入登录名" value="admin" class="layui-input">
                            </div>
                            <div class="layui-form-item">
                                <label class="beg-login-icon fs1">
                                    <i class="icon icon-key"></i>
                                </label>
                                <input type="password" name="password" lay-verify="required" placeholder="这里输入密码" value="admin123" class="layui-input">
                            </div>
                            <?php if($system['code'] == 'open'): ?>
                            <div class="layui-form-item">
                                <input type="text" name="vercode" id="captcha" lay-verify="required" placeholder="验证码" autocomplete="off" class="layui-input">
                                <span id="chkcode" style="width:50px; float:left;"></span>
                                <div class="captcha">
                                    <img src="<?php echo url('verify'); ?>" alt="captcha" onclick="this.src='<?php echo url("verify"); ?>?'+'id='+Math.random()"/>
                                </div>
                                
                            </div>
                            <?php endif; ?>
                            <div class="layui-form-item">
                                <button type="submit" class="layui-btn btn-submit btn-blog" lay-submit lay-filter="login">登录</button>
                            </div>
                        </form>
                    </div>
                    <footer>
                        <p><?php echo config('sys_name'); ?> © <?php echo config('url_domain_root'); ?></p>
                    </footer>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="/mycltphp/public/static/admin/js/rAF.js"></script>
<script src="/mycltphp/public/static/admin/js/login.js"></script>
<script type="text/javascript" src="/mycltphp/public/static/plugins/layui/layui.js"></script>
<script>
    layui.use('form',function(){
        var form = layui.form,$ = layui.jquery;
        //监听提交
        form.on('submit(login)', function(data){
            loading =layer.load(1, {shade: [0.1,'#fff'] });//0.1透明度的白色背景
            $.post('<?php echo url("login/index"); ?>',data.field,function(res){
                layer.close(loading);
                if(res.code == 1){
                    layer.msg(res.msg, {icon: 1, time: 1000}, function(){
                        location.href = "<?php echo url('admin/index/index'); ?>";
                    });
                }else{
                    $('#captcha').val('');
                    layer.msg(res.msg, {icon: 2, anim: 6, time: 1000});
                    $('.captcha img').attr('src','<?php echo url("verify"); ?>?id='+Math.random());
                }
            });
            return false;
        });
		
		$("#captcha").bind("input propertychange",function(){
		    var code = $(this).val();
			if(code == "" || code.length == "0"){
		       $(this).attr('placeholder','请输入验证码');
			   $(this).css('border-color','red');
			   $("#chkcode").html("<img src='/mycltphp/public/static/admin/images/cuo.png'>");
		   }else if(code.length != 4){
		       $(this).attr('placeholder','长度错误');
			   $(this).css('border-color','red');
			   $("#chkcode").html("<img src='/mycltphp/public/static/admin/images/cuo.png'>");
		   }else{
		      $.ajax({
			       url:"<?php echo url('checkVerify'); ?>",
				   dataType:"json",
				   data: {'code':$("#captcha").val()},
				   type:'get',
				   success:function(data){
				     if(data){
				         $("#chkcode").html("<img src='/mycltphp/public/static/admin/images/dh.png'>");
					  }else{
					     $("#chkcode").html("<img src='/mycltphp/public/static/admin/images/cuo.png'>");
					  }
				   }
			  });
		      $(this).css('border-color','#e2e2e2');
		   }
		})
    });
	if (window.top != window.self) {
		window.top.location.href = window.self.location.href;
	}
</script>
</body>
</html>